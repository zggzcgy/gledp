GLEDP
=====

#### 介绍

<p align="center">
Linux内核驱动学习,点亮一个LED灯，树莓派4B+，芯片为BCM2711<br/>
<a href="LICENSE"><img src="https://img.shields.io/badge/license-GPL--3.0-green" alt="License"></a>
<img src="https://img.shields.io/badge/platform-linux-lightgrey"/>
<img src="https://img.shields.io/badge/architecture-arm64-brightgreen"/>
</p>


#### 安装步骤

1.  `git clone https://gitee.com/zggzcgy/gledp.git`
2.  `cd gledp`
3.  `make`

加载到内核模块
```sh
sudo insmod led.ko
```
卸载led内核模块
```sh
sudo rmmod led.lo
```

#### 说明

目的是通过Linux内核操作寄存器完成LED灯的控制，并非直接使用上层应用[^1],若能熟练掌握，内容共涉及下面知识点。
1.  内核模块的创建和构建
2.  内核日志的打印
3.  位运算
4.  虚拟内存的映射
5.  树莓派4B寄存器操作

知识主要参考博客[自增人生][blog]，代码参考[github][code],由于PI3处理器和PI4不一样，需要参考BCM2711官方[数据手册][manual],本驱动默认使用27号针脚进行测试。官方给的针脚顺序如下图所示，这个版本主要给python这类在官方驱动提供参考顺序，我们不采用这个顺序。![pin27][gpio]

我们主要以正方形状为起始1，右边为2，成之字型从上往下数，相关连接如图所示。![图片](res/pin.svg)

[blog]: <https://ixx.life/RPiDriverInAction/01/> (GPIO驱动之LED)
[code]: <(https://github.com/Sojyu-Shirakawa/led_switch/blob/main/myled.c)>
[manual]: <https://datasheets.raspberrypi.com/bcm2711/bcm2711-peripherals.pdf>  "bcm2711"
[gpio]: <res/PI4-GPIO.png> "树莓派针脚图"

其它注意事项
1.  仅在树莓派4B上完成测试。
2.  所有操作均在树莓派上操作。
3.  如有其他疑问欢迎进行讨论学习。

### 许可信息
[GNU General Public License v3.0](LICENSE)

[^1]: 上层应用：指在用户层直接调用操作系统接口的应用程序，如Python等应用。
