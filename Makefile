ifneq ($(KERNELRELEASE),)

obj-m := led.o

else

PWD	:= $(shell pwd)
KDIR	:= /usr/lib/modules/`uname -r`/build
CROSS	:= #ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-

all:
	$(MAKE) -C $(KDIR) M=$(PWD) $(CROSS) modules
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) $(CROSS) clean
endif
